package VPEx9;

public class Proces {
    public static void main(String[] args) {
        Magazine produs= new Magazine();
        produs.setPufulet("Cristinel, Cristinuta");
        produs.setMagazin("Metro, Nr1, Linella");
        produs.setVehicol("Man 360LT");
        produs.setNumarMasina("CHX 455");
        System.out.println("Produsele urmatoarea : Pufuleti "+produs.getPufulet()+" vor fi trasportate" +
                "la depozitele urmatoarelor magazine "+produs.getMagazin()+" de vehicolul "+ produs.getVehicol()+
                " cu numarul de inmatriculare "+ produs.getNumarMasina());
    }
}
